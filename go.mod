module bitbucket.org/snapmartinc/upload-files

go 1.12

require (
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	bitbucket.org/snapmartinc/trace v0.0.0-20190925102910-0918afc8a51f // indirect
	bitbucket.org/snapmartinc/user-service-client v0.0.0-20190916112339-fefd7d2a4d59 // indirect
	github.com/aws/aws-sdk-go v1.21.9
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	gopkg.in/redis.v5 v5.2.9 // indirect
)
